#include "amigo.hpp"
#include <string>

amigo::amigo(){
	setNome("nulo");
	setIdade("Nula");
	setTelefone("vazio");
	facebook = "facebook.com/usuario";
	endereco = "endereco vazio";
}

void amigo::setFacebook(string facebook){
	this->facebook = facebook;
}

string amigo::getFacebook(){
	return facebook;
}

void amigo::setEndereco(string endereco){
	this->endereco = endereco;
}

string amigo::getEndereco(){
	return endereco;
}

