all:		pessoa.o main.o amigo.o
		g++ -o main pessoa.o main.o amigo.o
			
pessoa.o:	pessoa.cpp pessoa.hpp
		g++ -c pessoa.cpp

main.o:		main.cpp pessoa.hpp
			g++ -c main.cpp

amigo.o: 	amigo.cpp amigo.hpp
		g++ -c amigo.cpp
			
clean:		rm -rf *.o

run:		./main
