#include <iostream>
#include "pessoa.hpp"
#include "amigo.hpp"

using namespace std;

int main(){

	Pessoa umaPessoa;
	
	cout << "Nome: " << umaPessoa.getNome() << endl;
	cout << "Idade: " << umaPessoa.getIdade() << endl;
	
	umaPessoa.setNome("Paulo");
	umaPessoa.setIdade("32");
	umaPessoa.setTelefone("555-5555");
	
	cout << "Nome: " << umaPessoa.getNome() << endl;
	cout << "Idade: " << umaPessoa.getIdade() << endl;

	amigo umAmigo;

	umAmigo.setNome("Julio");
	umAmigo.setIdade("18");
	umAmigo.setTelefone("666-6666");
	umAmigo.setFacebook("facebook/julioxavierr");
	umAmigo.setEndereco("meu endereco");

	cout << "Nome: " << umAmigo.getNome() << endl;
	cout<< "Idade: " << umAmigo.getIdade()<< endl;
	cout << "Facebook: "<< umAmigo.getFacebook() << endl;
	cout << "Endereco: "<< umAmigo.getEndereco() << endl;
	
	return 0;
	}
