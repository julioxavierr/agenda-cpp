#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"
#include <string>

class amigo : public Pessoa{
private:
	string facebook;
	string endereco;
public:
	amigo();
	void setFacebook(string facebook);
	string getFacebook();
	void setEndereco(string endereco);
	string getEndereco();
};

#endif
